import java.util.Scanner;
public class HelloWorld {
    static void order(String foodName){
        System.out.println("You have ordered " + foodName + ".Thank you!");
        System.out.println("Thank you for ordering " + foodName + "! It will be served as soon as possible!");
        System.out.println("We have received order for " + foodName + "! It will be served soon!");
    }

    public static void main(String[] args) {
       String[] food = {"Tempura","Ramen","Udon"};
       Scanner UserIn = new Scanner(System.in);

       System.out.println("What would you like to order:");
       for(int i=0;i<3;i++){
           System.out.println((i+1)+"."+food[i]);
       }

       System.out.print("Your order[1-3]:");
       int index = UserIn.nextInt();
       order(food[index-1]);
    }
}
